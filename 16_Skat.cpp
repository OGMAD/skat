#include <vector>
#include <ctime>
#include <string>
#include <random>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <iomanip>

using namespace std;

//globale Variable Kartendeck
vector < string > kartendeck({ " [A-S] ", " [A-R] ", " [A-G] ", " [A-E] ", " [10-S] ", " [10-R] ", " [10-G] ", " [10-E] ", " [K-S] ", " [K-R] ", " [K-G] ", " [K-E] ", " [O-S] ", " [O-R] ", " [O-G] ", " [O-E] ", " [U-S] ", " [U-R] ", " [U-G] ", " [U-E] ", " [9-S] ", " [9-R] ", " [9-G] ", " [9-E] ", " [8-S] ", " [8-R] ", " [8-G] ", " [8-E] ", " [7-S] ", " [7-R] ", " [7-G] ", " [7-E] " });

//###########################################################################
//Beginn Klasse und Klassenunktionen Rund um das Spielfeld
class Spielfeld
{
public:
	void karteAufFeldLegen(string Karte);
	vector < string > kartenAufSpielfeldLesen();
	void kartenAufSpielfeldAusgeben();
	void kartenVonSpielfeldLoeschen();

private:
	vector < string > p_KartenAufDemFeld;
};

//Gibt die Karten, welche aktuell für das Stechen auf dem Spielfeld liegen, zurück.
vector < string > Spielfeld::kartenAufSpielfeldLesen()
{
	return p_KartenAufDemFeld;
}

//Legt eine Karte auf das Spielfeld
void Spielfeld::karteAufFeldLegen(string Karte)
{
	p_KartenAufDemFeld.push_back(Karte);
}

//Entfernt alle Karten vom Spielfeld
void Spielfeld::kartenVonSpielfeldLoeschen()
{
	p_KartenAufDemFeld.clear();
}

//Gibt auf der Konsole aus, welche Karten sich aktuell auf dem Spielfeld befinden
void Spielfeld::kartenAufSpielfeldAusgeben()
{
	cout << "\n######################################################";
	cout << "\nAuf dem Spielfeld liegen:\n";

	for (int i = p_KartenAufDemFeld.size(); i > 0; i--)
	{
		cout << p_KartenAufDemFeld.at(i - 1);
	}

	cout << "\n######################################################";
}
//Ende Klasse und Klassenunktionen Rund um das Spielfeld
//###########################################################################
//###########################################################################
//Beginn Klasse und Klassenunktionen Rund um den Spieler
class Spieler
{
public:
	Spieler(string name);
	~Spieler() = default;
	void Zug(bool rundenbeginn, char ersteFarbeDerRunde);
	void spielerHandSchreiben(vector < string >);
	string gespielteKarteLesen();
	int wertGespielteKarteLesen();
	void spielerNameAusgeben();
	void gestocheneKartenSchreiben(vector < string > karten);
	void gestocheneKartenAusgeben();
	void ersterZug(bool ersterZug);
	bool ersterZugLesen();
	char ersteFarbeDerRundeLesen();
	int kartenanzahlSpielerhandLesen();
	int gewinner(int spielernummer, vector < string > kartenAufFeld);
	int punkteZaehlen();
	int punkteLesen();
	void trumpffarbeSchreiben(char trumpffarbe);
	char trumpffabeLesen();
private:
	string p_Name;
	vector < string > p_SpielerHand;
	vector < string > p_GestocheneKarten;
	int p_GespielteKartePosition;
	string p_GespielteKarte;
	int p_WertGespielteKarte;
	char p_Trumpf;
	char p_Kartenfarbe;
	char p_Kartentyp;
	char p_ErsteFarbeDerRunde;
	bool p_KorrekteFarbe;
	bool p_ErsterZugDesSpiels;
	bool p_KorrekteFarbeGespielt;
	bool p_GespielteKartePositionKorrekt;
	int p_Punkte;
};

Spieler::Spieler(string name)
{
	p_Name = name;
}

//Gewinner des Stichs ausgeben und dem Gewinner die gestochenen Karten geben.
int Spieler::gewinner(int spielernummer, vector < string > kartenAufFeld)
{
	cout << "\n\n\n######################################################\n";
	cout << p_Name << " hat den Stich gewonnen!\n";
	cout << "######################################################\n\n\n";

	for (int i = 0; i < 3; i++)
	{
		p_GestocheneKarten.push_back(kartenAufFeld.back());
		kartenAufFeld.pop_back();
	}

	return spielernummer;
}

/*Gibt die Anzahl der Karten auf der Hand des Spielers zurück.
Diese wird benötigt um festzustellen wann der letzte Stich ist.*/
int Spieler::kartenanzahlSpielerhandLesen()
{
	return p_SpielerHand.size();
}

/*Gibt die Erste gespielte Farbe der Runde zurück,
um dem anderen Spielern mitzuteilen welche Farbe bedient werden muss.*/
char Spieler::ersteFarbeDerRundeLesen()
{
	return p_ErsteFarbeDerRunde;
}

/*Übergibt dem Spieler die Information ob er den ersten Zug des Spieles macht.
Dies wird benötigt um die Trumpffarbe zu bestimmen.*/
void Spieler::ersterZug(bool ersterZug)
{
	p_ErsterZugDesSpiels = ersterZug;
}


//Spielzug eines Spielers
void Spieler::Zug(bool rundenbeginn, char ersteFarbeDerRunde)
{
	//übergibt welche Farbe bedient werden muss
	p_ErsteFarbeDerRunde = ersteFarbeDerRunde;

	cout << "\n" << p_Name << " ist am Zug!\n";
	if (rundenbeginn == false)
	{
		cout << p_ErsteFarbeDerRunde << " ist zu bedienen!\n";
	}

	do
	{
		do {
			cout << "Du hast folgende Karten auf der Hand:\n\n";

			//Spielerhand ausgeben
			for (int i = p_SpielerHand.size(); i > 0; i--)
			{
				cout << setw(8) << p_SpielerHand.at(i - 1);
			}

			cout << "\n";
			//Positionen der Karten auf der Hand...bzw die Zahl die gedrückt werden muss um die Karte zu spielen
			vector < string > legende({ " 1   "," 2   "," 3   "," 4   "," 5   "," 6   "," 7   "," 8   "," 9   "," 10  " });

			for (int i = p_SpielerHand.size(); i > 0; i--)
			{
				cout << setw(8) << legende.at(i - 1);
			}

			//Eingabe welche Karte gespielt werden soll und Prüfung auf Korrektheit der Eingabe
			p_GespielteKartePositionKorrekt = false;

			cout << "\n\n Die wievielte Karte von rehts gesehen moechtest du spielen? \nDie rechteste Karte entspricht der Zahl 1. \n";
			cin >> p_GespielteKartePosition;

			//Prüfung auf Korrektheit der Eingabe
			for (int i = 1; i <= p_SpielerHand.size(); i++)
			{
				if (i == p_GespielteKartePosition)
				{
					p_GespielteKartePositionKorrekt = true;
				}
			}

			if (p_GespielteKartePositionKorrekt == false)
			{
				cout << "\n#####################################################################\n";
				cout << "Die Eingabe ist fehlerhaft! bitte geben Sie eine Zahl von 1-10 ein! \nEs muss sich eine Karte fuer die entsprechende Zahl auf Ihrer Hand befinden!\n";
				cout << "#####################################################################\n\n";
			}


			if (p_GespielteKartePositionKorrekt == false)
			{
				cin.clear();
				cin.ignore(numeric_limits<streamsize>::max(), '\n');
			}

		} while (p_GespielteKartePositionKorrekt == false);

		p_GespielteKarte = p_SpielerHand.at((p_GespielteKartePosition)-1);
		cout << "Du spielst:" << p_GespielteKarte;


		//Kartentyp und Kartenfarbe bestimmen
		vector<char> cstr(p_GespielteKarte.c_str(), p_GespielteKarte.c_str() + p_GespielteKarte.size() + 1);

		p_Kartentyp = cstr.at(2);
		p_Kartenfarbe = cstr.at(4);

		if (p_Kartenfarbe == '-')
		{
			p_Kartenfarbe = cstr.at(5);
		}

		//Prüfung ob Spieler die zu bedienene Farbe auf der Hand hat
		bool p_KorrekteFarbeNichtAufHand = true;
		for (int i = ((p_SpielerHand.size()) - 1); i >= 0; i--)
		{
			//Ermittlung der Kartenfarbe
			vector<char> cstr(p_SpielerHand.at(i).c_str(), p_SpielerHand.at(i).c_str() + p_SpielerHand.at(i).size() + 1); 
			char farbe = cstr.at(4);

			if (farbe == '-')
			{
				farbe = cstr.at(5);
			}
			if (farbe == ersteFarbeDerRunde)
			{
				p_KorrekteFarbeNichtAufHand = false;
			}
		}

		//Kontrolle ob richtige Farbe gespielt wurde
		if ((rundenbeginn == true) || (ersteFarbeDerRunde == p_Kartenfarbe) || (p_KorrekteFarbeNichtAufHand == true))
		{
			p_KorrekteFarbeGespielt = true;
			p_KorrekteFarbeNichtAufHand = false;
		}
		else
		{
			p_KorrekteFarbeGespielt = false;
			cout << "\n\n##########################################";
			cout << "\nDu hast die falsche Farbe gespielt!\n";
			cout << "\n" << "Du hast " << p_Kartenfarbe << " gespielt, aber " << ersteFarbeDerRunde << " muss bedient werden!\n";
			cout << "##########################################\n\n";
		}
	} while (p_KorrekteFarbeGespielt == false);

	//Trumpf bestimmen
	if (p_ErsterZugDesSpiels == true)
	{
		p_Trumpf = p_Kartenfarbe;
		cout << "\n" << p_Trumpf << " ist Trumpf! \n \n";
	}
	p_ErsterZugDesSpiels = false;

	//Bestimmung der zu bedienenen Farbe
	if (rundenbeginn == true)
	{
		p_ErsteFarbeDerRunde = p_Kartenfarbe;
	}

	//Prüfung ob die richtige Farbe gespielt wurde
	if (p_ErsteFarbeDerRunde == p_Kartenfarbe)
	{
		p_KorrekteFarbe = true;
	}
	else
	{
		p_KorrekteFarbe = false;
	}

	//Kartenwert für Stich betimmen

	if (p_Kartentyp == 'A')
	{
		p_WertGespielteKarte = 11;
		if (p_Kartenfarbe == p_Trumpf)
		{
			p_WertGespielteKarte = p_WertGespielteKarte + 20;
		}
	}
	if (p_Kartentyp == '1')
	{
		p_WertGespielteKarte = 10;
		if (p_Kartenfarbe == p_Trumpf)
		{
			p_WertGespielteKarte = p_WertGespielteKarte + 20;
		}
	}
	if (p_Kartentyp == 'K')
	{
		p_WertGespielteKarte = 4;
		if (p_Kartenfarbe == p_Trumpf)
		{
			p_WertGespielteKarte = p_WertGespielteKarte + 20;
		}
	}
	if (p_Kartentyp == 'O')
	{
		p_WertGespielteKarte = 3;
		if (p_Kartenfarbe == p_Trumpf)
		{
			p_WertGespielteKarte = p_WertGespielteKarte + 20;
		}
	}
	if (p_Kartentyp == 'U')
	{
		p_WertGespielteKarte = 2;
		if (p_Kartenfarbe == p_Trumpf)
		{
			p_WertGespielteKarte = p_WertGespielteKarte + 20;
		}
	}
	if (p_Kartentyp == '9')
	{
		p_WertGespielteKarte = 0;
		if (p_Kartenfarbe == p_Trumpf)
		{
			p_WertGespielteKarte = p_WertGespielteKarte + 20;
		}
	}
	if (p_Kartentyp == '8')
	{
		p_WertGespielteKarte = 0;
		if (p_Kartenfarbe == p_Trumpf)
		{
			p_WertGespielteKarte = p_WertGespielteKarte + 20;
		}
	}
	if (p_Kartentyp == '7')
	{
		p_WertGespielteKarte = 0;
		if (p_Kartenfarbe == p_Trumpf)
		{
			p_WertGespielteKarte = p_WertGespielteKarte + 20;
		}
	}
	if ((p_KorrekteFarbe == false) && (p_Kartenfarbe != p_Trumpf))
	{
		p_WertGespielteKarte = 0;
	}

	cout << "Der Wert dieser Karte betraegt: " << p_WertGespielteKarte;

	//Einzelne Karte von Hand löschen
	p_SpielerHand.erase(p_SpielerHand.begin() + p_GespielteKartePosition - 1);
}

/*gibt zurück ob der erste Spielzug bereits stattgefunden hat,
wird benötigt um den Trumpf zu bestimmen*/
bool Spieler::ersterZugLesen()
{
	return p_ErsterZugDesSpiels;
}

//Ausgabe des vom Spieler, zu Beginn des Spiels, eingegebenen Namens
void Spieler::spielerNameAusgeben()
{
	cout << p_Name;
}

//Das Übergeben der Handkarten an den Spieler
void Spieler::spielerHandSchreiben(vector < string > kartenFuerHand)
{
	p_SpielerHand = kartenFuerHand;
}

//Gibt die Karten aus, welche der Spieler gestochen hat
void Spieler::gestocheneKartenAusgeben()
{
	for (int i = p_GestocheneKarten.size(); i > 0; i--)
	{
		cout << p_GestocheneKarten.at(i - 1);
	}
}

//fügt dem "Stapel", der vom Spieler gestochenen Karten, die Karten des letzten Stiches hinzu
void Spieler::gestocheneKartenSchreiben(vector <string> karten)
{
	for (int i = 0; i < 2; i++)
	{
		p_GestocheneKarten.push_back(karten.back());
	}
}

//gibt die aktuell vom Spieler auf das Spielfeld gelegte Karte zurück
string Spieler::gespielteKarteLesen()
{
	return p_GespielteKarte;
}

//Gibt den Wert der aktuell vom Spieler auf das Spielfeld gelegte Karte zurück
int Spieler::wertGespielteKarteLesen()
{
	return p_WertGespielteKarte;
}

//zählt die Augen der vom Spieler insgesamt gestochenen Karten zusammen
int Spieler::punkteZaehlen()
{
	p_Punkte = 0;

	/*Die Strings der einzelnen Karten werden zerlegt und die einzelnen Chars, 
	welche Aussagen über den Typ der Karte treffen,
	im Vektor "zwischenspeicher" abgelegt.*/
	vector <char> zwischenspeicher;

	for (int i = ((p_GestocheneKarten.size()) - 1); i >= 0; i--)
	{
		vector<char> cstr(p_GestocheneKarten.at(i).c_str(), p_GestocheneKarten.at(i).c_str() + p_GestocheneKarten.at(i).size() + 1);

		zwischenspeicher.push_back(cstr.at(2));
	}

	//zählt die Augen zusammen
	for (int i = ((zwischenspeicher.size()) - 1); i >= 0; i--)
	{
		if (zwischenspeicher.at(i) == '7')
		{
			p_Punkte = p_Punkte + 7;
		}
		if (zwischenspeicher.at(i) == '8')
		{
			p_Punkte = p_Punkte + 8;
		}
		if (zwischenspeicher.at(i) == '9')
		{
			p_Punkte = p_Punkte + 9;
		}
		if (zwischenspeicher.at(i) == '1')
		{
			p_Punkte = p_Punkte + 10;
		}
		if (zwischenspeicher.at(i) == 'U')
		{
			p_Punkte = p_Punkte + 2;
		}
		if (zwischenspeicher.at(i) == 'O')
		{
			p_Punkte = p_Punkte + 3;
		}
		if (zwischenspeicher.at(i) == 'K')
		{
			p_Punkte = p_Punkte + 4;
		}
		if (zwischenspeicher.at(i) == 'A')
		{
			p_Punkte = p_Punkte + 11;
		}
	}

	return p_Punkte;
}

//gibt die Augen der vom Spieler insgesamt gestochenen Karten zurück
int Spieler::punkteLesen()
{
	return p_Punkte;
}

//übergibt die Trumpffarbe an den Spieler
void Spieler::trumpffarbeSchreiben(char trumpffarbe)
{
	p_Trumpf = trumpffarbe;
}

//gibt die Trumpffarbe zurück
char Spieler::trumpffabeLesen()
{
	return p_Trumpf;
}
//Ende Klasse und Klasenfunktionen Rund um den Spieler
//###########################################################################
//###########################################################################
//Beginn Funktionen zum Karten und Spielernamen verteilen vor Spielbeginn

//mischt den globalen Vektor "Kartendeck" zufällig durch
vector < string > kartenMischen();
vector < string > kartenMischen()
{
	random_device rd;
	mt19937 g(rd());

	shuffle(kartendeck.begin(), kartendeck.end(), g);
	return kartendeck;
}

//entnimmt dem globalen Vektor "Kartendeck" das Skat
vector < string > skatWegLegen();
vector < string > skatWegLegen()
{
	vector < string > Skat;

	for (int i = 0; i < 2; i++)
	{
		Skat.push_back(kartendeck.back());
		kartendeck.pop_back();
	}
	return Skat;
}

//ermittelt zufällig, welcher Spieler das Spiel zu beginnen hat
unsigned short int ersterZug();
unsigned short int ersterZug()
{
	default_random_engine randomNumber(time(0));
	uniform_int_distribution <int> ersterZug(1, 3);
	return ersterZug(randomNumber);
}

//entnimmt dem globalen Vektor "Kartendeck" die Handkarten für einen Spieler und gibt diese zurück
vector < string > kartenVerteilen();
vector < string > kartenVerteilen()
{
	vector < string > handKarten;

	for (int i = 0; i < 10; i++)
	{
		handKarten.push_back(kartendeck.back());
		kartendeck.pop_back();
	}
	return handKarten;
}

//Ende Funktionen zum Karten und Spielernamen verteilen vor Spielbeginn
//############################################################################

int main()
{
	cout << "Das Spiel heisst Skat!\n" << "Es wird im Ramsch gespielt, wer die wenigsten Augen sammelt gewinnt!\n" << "Wer den letzten Stich macht erhaelt das Skat!\n\n";

	//Spielbeginn
	bool ersterZugDesSpiels = true;
	Spielfeld Spielfeld;

	//Bedingung für die while Schleife, welche durchläuft solange das Spiel nicht beendet ist
	bool spielLaeuft = true;

	kartendeck = kartenMischen();
	vector < string > Skat{ (skatWegLegen()) };

	//Die Spieler können ihre Namen eingeben und bekommen Handkarten
	string spielernamen;

	cout << " Name Spieler 1: "; cin >> spielernamen;
	Spieler Spieler1(spielernamen);
	Spieler1.spielerHandSchreiben(kartenVerteilen());

	cout << " Name Spieler 2: "; cin >> spielernamen;
	Spieler Spieler2(spielernamen);
	Spieler2.spielerHandSchreiben(kartenVerteilen());

	cout << " Name Spieler 3: "; cin >> spielernamen;
	Spieler Spieler3(spielernamen);
	Spieler3.spielerHandSchreiben(kartenVerteilen());

	//Wer den ersten Zug machen darf wird zufällig bestimmt
	int gewinnerLetzterStich = ersterZug();
	
	//Wird benötigt um 1-2 If-Schleifen zu überspringen, je nachdem welcher Spieler den Zug beginnt
	bool liefSchon = false;

	//Wird benötigt um dem Gewinner des letzten Stiches das Skat zu geben
	bool letzterStichDesSpiels = false;

	//Wird benötigt um an die anderen Spieler die Trumpffarbe zu übergeben
	char trumpffarbe;

	//Wird benötigt um den Trumpf zu bestimmen
	Spieler1.ersterZug(true);
	Spieler2.ersterZug(true);
	Spieler3.ersterZug(true);

	//Spielablauf
	do
	{
		if ((gewinnerLetzterStich == 1) && (liefSchon == false))
		{
			//Zug Spieler 1
			if (Spieler1.ersterZugLesen() == false)
			{
				Spieler1.trumpffarbeSchreiben(trumpffarbe);
			}

			Spieler1.Zug(true, 'X');
			Spielfeld.karteAufFeldLegen(Spieler1.gespielteKarteLesen());

			//Trumpfbestimmung
			trumpffarbe = Spieler1.trumpffabeLesen();
			Spieler1.ersterZug(false);
			Spieler2.ersterZug(false);
			Spieler3.ersterZug(false);


			//Zug Spieler 2
			Spieler2.trumpffarbeSchreiben(trumpffarbe);
			Spielfeld.kartenAufSpielfeldAusgeben();
			Spieler2.Zug(false, Spieler1.ersteFarbeDerRundeLesen());
			Spielfeld.karteAufFeldLegen(Spieler2.gespielteKarteLesen());

			//Zug Spieler 3
			Spieler3.trumpffarbeSchreiben(trumpffarbe);
			Spielfeld.kartenAufSpielfeldAusgeben();
			Spieler3.Zug(false, Spieler1.ersteFarbeDerRundeLesen());
			Spielfeld.karteAufFeldLegen(Spieler3.gespielteKarteLesen());

			//Berechnung Gewinner des Stiches...geben der gestochenen Karten an den Stichgewinner...merken wer den Stich gewonnen hat
			if (((Spieler1.wertGespielteKarteLesen() == Spieler2.wertGespielteKarteLesen()) && (Spieler1.wertGespielteKarteLesen() == Spieler3.wertGespielteKarteLesen())) || ((Spieler1.wertGespielteKarteLesen() > Spieler2.wertGespielteKarteLesen()) && (Spieler1.wertGespielteKarteLesen() > Spieler3.wertGespielteKarteLesen())))
			{
				gewinnerLetzterStich = Spieler1.gewinner(1, Spielfeld.kartenAufSpielfeldLesen());
			}

			if ((Spieler2.wertGespielteKarteLesen() > Spieler1.wertGespielteKarteLesen()) && (Spieler2.wertGespielteKarteLesen() > Spieler3.wertGespielteKarteLesen()))
			{
				gewinnerLetzterStich = Spieler2.gewinner(2, Spielfeld.kartenAufSpielfeldLesen());
			}

			if ((Spieler3.wertGespielteKarteLesen() > Spieler2.wertGespielteKarteLesen()) && (Spieler3.wertGespielteKarteLesen() > Spieler1.wertGespielteKarteLesen()))
			{
				gewinnerLetzterStich = Spieler3.gewinner(3, Spielfeld.kartenAufSpielfeldLesen());
			}
			//Ende des Stiches
			Spielfeld.kartenVonSpielfeldLoeschen();
			liefSchon = true;
		}

		if ((gewinnerLetzterStich == 2) && (liefSchon == false))
		{
			//Zug Spieler 2
			if (Spieler2.ersterZugLesen() == false)
			{
				Spieler2.trumpffarbeSchreiben(trumpffarbe);
			}

			Spieler2.Zug(true, 'X');
			Spielfeld.karteAufFeldLegen(Spieler2.gespielteKarteLesen());

			//Trumpfbestimmung
			trumpffarbe = Spieler2.trumpffabeLesen();
			Spieler1.ersterZug(false);
			Spieler2.ersterZug(false);
			Spieler3.ersterZug(false);


			//Zug Spieler 3
			Spieler3.trumpffarbeSchreiben(trumpffarbe);
			Spielfeld.kartenAufSpielfeldAusgeben();
			Spieler3.Zug(false, Spieler2.ersteFarbeDerRundeLesen());
			Spielfeld.karteAufFeldLegen(Spieler3.gespielteKarteLesen());

			//Zug Spieler 1
			Spieler1.trumpffarbeSchreiben(trumpffarbe);
			Spielfeld.kartenAufSpielfeldAusgeben();
			Spieler1.Zug(false, Spieler2.ersteFarbeDerRundeLesen());
			Spielfeld.karteAufFeldLegen(Spieler1.gespielteKarteLesen());

			//Berechnung Gewinner des Stiches...geben der gestochenen Karten an den Stichgewinner...merken wer den Stich gewonnen hat
			if ((Spieler1.wertGespielteKarteLesen() > Spieler2.wertGespielteKarteLesen()) && (Spieler1.wertGespielteKarteLesen() > Spieler3.wertGespielteKarteLesen()))
			{
				gewinnerLetzterStich = Spieler1.gewinner(1, Spielfeld.kartenAufSpielfeldLesen());
			}

			if (((Spieler1.wertGespielteKarteLesen() == Spieler2.wertGespielteKarteLesen()) && (Spieler1.wertGespielteKarteLesen() == Spieler3.wertGespielteKarteLesen())) || ((Spieler2.wertGespielteKarteLesen() > Spieler1.wertGespielteKarteLesen()) && (Spieler2.wertGespielteKarteLesen() > Spieler3.wertGespielteKarteLesen())))
			{
				gewinnerLetzterStich = Spieler2.gewinner(2, Spielfeld.kartenAufSpielfeldLesen());
			}

			if ((Spieler3.wertGespielteKarteLesen() > Spieler2.wertGespielteKarteLesen()) && (Spieler3.wertGespielteKarteLesen() > Spieler1.wertGespielteKarteLesen()))
			{
				gewinnerLetzterStich = Spieler3.gewinner(3, Spielfeld.kartenAufSpielfeldLesen());
			}
			//Ende des Stiches
			Spielfeld.kartenVonSpielfeldLoeschen();
			liefSchon = true;
		}

		if ((gewinnerLetzterStich == 3) && (liefSchon == false))
		{
			//Zug Spieler 3
			if (Spieler3.ersterZugLesen() == false)
			{
				Spieler3.trumpffarbeSchreiben(trumpffarbe);
			}

			Spieler3.Zug(true, 'X');
			Spielfeld.karteAufFeldLegen(Spieler3.gespielteKarteLesen());

			//Trumpfbestimmung
			trumpffarbe = Spieler3.trumpffabeLesen();
			Spieler1.ersterZug(false);
			Spieler2.ersterZug(false);
			Spieler3.ersterZug(false);


			//Zug Spieler 1
			Spieler1.trumpffarbeSchreiben(trumpffarbe);
			Spielfeld.kartenAufSpielfeldAusgeben();
			Spieler1.Zug(false, Spieler3.ersteFarbeDerRundeLesen());
			Spielfeld.karteAufFeldLegen(Spieler1.gespielteKarteLesen());

			//Zug Spieler 2
			Spieler2.trumpffarbeSchreiben(trumpffarbe);
			Spielfeld.kartenAufSpielfeldAusgeben();
			Spieler2.Zug(false, Spieler3.ersteFarbeDerRundeLesen());
			Spielfeld.karteAufFeldLegen(Spieler2.gespielteKarteLesen());

			if (Spieler1.kartenanzahlSpielerhandLesen() == 0)
			{
				letzterStichDesSpiels = true;
			}

			//Berechnung Gewinner des Stiches...geben der gestochenen Karten an den Stichgewinner...merken wer den Stich gewonnen hat
			if ((Spieler1.wertGespielteKarteLesen() > Spieler2.wertGespielteKarteLesen()) && (Spieler1.wertGespielteKarteLesen() > Spieler3.wertGespielteKarteLesen()))
			{
				gewinnerLetzterStich = Spieler1.gewinner(1, Spielfeld.kartenAufSpielfeldLesen());
				if (letzterStichDesSpiels == true)
				{
					Spieler1.gestocheneKartenSchreiben(Skat);
				}
			}

			if ((Spieler2.wertGespielteKarteLesen() > Spieler1.wertGespielteKarteLesen()) && (Spieler2.wertGespielteKarteLesen() > Spieler3.wertGespielteKarteLesen()))
			{
				gewinnerLetzterStich = Spieler2.gewinner(2, Spielfeld.kartenAufSpielfeldLesen());
				if (letzterStichDesSpiels == true)
				{
					Spieler1.gestocheneKartenSchreiben(Skat);
				}
			}

			if (((Spieler1.wertGespielteKarteLesen() == Spieler2.wertGespielteKarteLesen()) && (Spieler1.wertGespielteKarteLesen() == Spieler3.wertGespielteKarteLesen())) || ((Spieler3.wertGespielteKarteLesen() > Spieler2.wertGespielteKarteLesen()) && (Spieler3.wertGespielteKarteLesen() > Spieler1.wertGespielteKarteLesen())))
			{
				gewinnerLetzterStich = Spieler3.gewinner(3, Spielfeld.kartenAufSpielfeldLesen());
				if (letzterStichDesSpiels == true)
				{
					Spieler1.gestocheneKartenSchreiben(Skat);
				}
			}
			//Ende des Stiches
			Spielfeld.kartenVonSpielfeldLoeschen();
			liefSchon = true;
		}
		liefSchon = false;
		ersterZugDesSpiels = false;

		//Ende des Spieles
		if ((Spieler1.kartenanzahlSpielerhandLesen() == 0) && (Spieler1.kartenanzahlSpielerhandLesen() == 0) && (Spieler1.kartenanzahlSpielerhandLesen() == 0))
		{
			//Ausgabe der gestochenen Karten und Berechnung der Punkte
			spielLaeuft = false;
			Spieler1.spielerNameAusgeben();
			cout << " hat folgende Karten gestochen:\n\n";
			Spieler1.gestocheneKartenAusgeben();
			cout << "\n\n";
			cout << "Das sind " << Spieler1.punkteZaehlen() << " Punkte!\n";

			Spieler2.spielerNameAusgeben();
			cout << " hat folgende Karten gestochen:\n\n";
			Spieler2.gestocheneKartenAusgeben();
			cout << "\n\n";
			cout << "Das sind " << Spieler2.punkteZaehlen() << " Punkte!\n";

			Spieler3.spielerNameAusgeben();
			cout << " hat folgende Karten gestochen:\n\n";
			Spieler3.gestocheneKartenAusgeben();
			cout << "\n\n";
			cout << "Das sind " << Spieler3.punkteZaehlen() << " Punkte!\n";

			//Bekanntgabe des, bzw der, Gewinner
			if ((Spieler1.punkteLesen() < Spieler2.punkteLesen()) && (Spieler1.punkteLesen() < Spieler3.punkteLesen()))
			{
				Spieler1.spielerNameAusgeben();
				cout << " hat das Spiel gewonnen!";
			}

			if ((Spieler2.punkteLesen() < Spieler1.punkteLesen()) && (Spieler2.punkteLesen() < Spieler3.punkteLesen()))
			{
				Spieler2.spielerNameAusgeben();
				cout << " hat das Spiel gewonnen!";
			}

			if ((Spieler3.punkteLesen() < Spieler2.punkteLesen()) && (Spieler3.punkteLesen() < Spieler1.punkteLesen()))
			{
				Spieler3.spielerNameAusgeben();
				cout << " hat das Spiel gewonnen!";
			}

			if ((Spieler1.punkteLesen() == Spieler2.punkteLesen()) && (Spieler1.punkteLesen() == Spieler3.punkteLesen()))
			{
				cout << "Es herrscht Gleichstand zwischen den Spielern!";
			}

			if ((Spieler1.punkteLesen() == Spieler2.punkteLesen()) && (Spieler1.punkteLesen() < Spieler3.punkteLesen()))
			{
				Spieler1.spielerNameAusgeben();
				cout << " und ";
				Spieler2.spielerNameAusgeben();
				cout << " haben gewonnen!";
			}

			if ((Spieler1.punkteLesen() == Spieler3.punkteLesen()) && (Spieler1.punkteLesen() < Spieler2.punkteLesen()))
			{
				Spieler1.spielerNameAusgeben();
				cout << " und ";
				Spieler3.spielerNameAusgeben();
				cout << " haben gewonnen!";
			}

			if ((Spieler3.punkteLesen() == Spieler2.punkteLesen()) && (Spieler3.punkteLesen() < Spieler1.punkteLesen()))
			{
				Spieler3.spielerNameAusgeben();
				cout << " und ";
				Spieler2.spielerNameAusgeben();
				cout << " haben gewonnen!";
			}
		}
	} while (spielLaeuft == true);
}
